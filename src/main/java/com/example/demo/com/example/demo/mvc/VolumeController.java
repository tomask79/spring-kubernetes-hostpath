package com.example.demo.com.example.demo.mvc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.UUID;

@RestController
public class VolumeController {

    @GetMapping( path = "/logUrlHit")
    public String logUrlHit() {
        final String fileNameRandom = UUID.randomUUID().toString();
        try (PrintWriter p = new PrintWriter(new FileOutputStream("/mnt/share/"+fileNameRandom+".txt",
                true))) {
            p.println("Datetime: "+new Date());
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }

        return "Url hit logged as "+fileNameRandom;
    }
}
